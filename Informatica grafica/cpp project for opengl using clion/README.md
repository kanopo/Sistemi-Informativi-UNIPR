# Istruzioni di utilizzo

per avviare uno dei file che il prof fornisce durante le esercitazioni, piazzarlo nella cartella `/src/InformaticaGrafica/metti_file_qui/`.


Cerca di avere un solo file cpp alla volta in quella cartella, così da evitare errori di compilazioni.

## quando usi gli shaders

usa il path assoluto perchè non va nulla altrimenti:

per esempio in `es3_coccarda_italia.cpp` ho messo:
```cpp
    shaderProgram = createShaderProgram("/home/me/Documents/Sistemi-Informativi-UNIPR/Informatica grafica/cpp project for opengl using clion/src/InformaticaGrafica/metti_file_qui/es3_coccarda_italia.vert","/home/me/Documents/Sistemi-Informativi-UNIPR/Informatica grafica/cpp project for opengl using clion/src/InformaticaGrafica/metti_file_qui/es3_coccarda_italia.frag");
```

e nel file di `create_shader_program.h` ho messo al posto di:
```cpp
std::string this_path = "str/";
```
ho inserito:
```cpp
std::string this_path = "";
```