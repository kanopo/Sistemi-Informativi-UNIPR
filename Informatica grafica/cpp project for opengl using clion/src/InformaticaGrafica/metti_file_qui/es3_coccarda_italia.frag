#version 330 core
in vec2 vPosXY;
out vec4 FragColor;
void main()
{
   float d = length(vPosXY);
   if (d < 0.33)
     FragColor = vec4(0.0,0.7,0.0,1.0);
   else if (d < 0.66)
     FragColor = vec4(1.0,1.0,1.0,1.0);
   else if (d < 1.0)
     FragColor = vec4(0.7,0.0,0.0,1.0);
   else discard;
}