#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cmath>

#include "create_shader_program.h"
#include "init_window.h"

GLfloat flag_verts[] = {
    -0.5f,  0.5f, 0.0f,
    -0.5f, -0.5f, 0.0f,

    0.5f,  0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
};
GLuint indices[] = {
    0, 1, 2,
    1, 3, 2,
};
unsigned int vbo_flag, vao_flag, ebo_flag;
int shaderProgram;
GLint radiusUniformLocation;
GLint translationUniformLocation;

float square_translation_x;
float square_translation_y;

void display(GLFWwindow* window)
{
    // render
    // ------
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(shaderProgram);
    glBindVertexArray(vao_flag);
    glUniform1f(radiusUniformLocation, 0.5f);
    glUniform2f(translationUniformLocation, square_translation_x, square_translation_y);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid *)0);

    glfwSwapBuffers(window);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    display(window);
}

void window_refresh_callback(GLFWwindow* window)
{
    display(window);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

void processKeyboard(GLFWwindow* window, double time_diff)
{
    double delta_x = 0.0;
    double delta_y = 0.0;
    const double speed = 0.5;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        delta_y = 1.0;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        delta_x = -1.0;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        delta_y = -1.0;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        delta_x = 1.0;
    delta_y *= speed * time_diff;
    delta_x *= speed * time_diff;

    if (square_translation_x + delta_x > -1.0 && square_translation_x + delta_x < 1.0)
        square_translation_x += float(delta_x);
    if (square_translation_y + delta_y > -1.0 && square_translation_y + delta_y < 1.0)
        square_translation_y += float(delta_y);
}

void advance(GLFWwindow* window, double time_diff)
{
    processKeyboard(window, time_diff);
}

// settings
const unsigned int SCR_WIDTH = 600;
const unsigned int SCR_HEIGHT = 600;

int main()
{
    GLFWwindow * window = init_window(SCR_WIDTH, SCR_HEIGHT, "Coccarda Italia");

    // callbacks
    // ---------
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetWindowRefreshCallback(window, window_refresh_callback);
    glfwSetKeyCallback(window, key_callback);

    // build and compile our shader program
    // ------------------------------------
    shaderProgram = createShaderProgram("/home/me/Documents/Sistemi-Informativi-UNIPR/Informatica grafica/cpp project for opengl using clion/src/InformaticaGrafica/metti_file_qui/es3_coccarda_italia.vert","/home/me/Documents/Sistemi-Informativi-UNIPR/Informatica grafica/cpp project for opengl using clion/src/InformaticaGrafica/metti_file_qui/es3_coccarda_italia.frag");

    glGenVertexArrays(1, &vao_flag);
    glGenBuffers(1, &vbo_flag);
    glGenBuffers(1, &ebo_flag);

    // load all data on vbo_flag
    glBindBuffer(GL_ARRAY_BUFFER, vbo_flag);
    glBufferData(GL_ARRAY_BUFFER, sizeof(flag_verts), flag_verts, GL_STATIC_DRAW);

    // load all data on ebo_flag
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_flag);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(vao_flag);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_flag);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),(void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_flag);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // uncomment this call to draw in wireframe polygons.
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    radiusUniformLocation = glGetUniformLocation(shaderProgram,"radius");
    translationUniformLocation = glGetUniformLocation(shaderProgram,"translation");

    square_translation_x = 0.0;
    square_translation_y = 0.0;

    // render loop
    // -----------
    double curr_time = glfwGetTime();
    double prev_time;
    while (!glfwWindowShouldClose(window))
    {
        display(window);
        glfwWaitEventsTimeout(0.01);

        prev_time = curr_time;
        curr_time = glfwGetTime();
        advance(window, curr_time - prev_time);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &vao_flag);
    glDeleteBuffers(1, &vbo_flag);
    glDeleteBuffers(1, &ebo_flag);

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}
