#version 330 core
layout (location = 0) in vec3 aPos;
uniform float radius;
out vec2 vPosXY;

uniform vec2 translation;

void main()
{
   vPosXY = aPos.xy / radius;
   gl_Position = vec4(aPos.xy + translation, aPos.z, 1.0);
}
